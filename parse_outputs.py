# Generates an output.json representing the output of the simulator.

import copy
import re
import os
import json
from collections import OrderedDict
from occam import Occam
from tools.file_tools.process_input_files import ProcessInputFiles

object = Occam.load()

job_path        = os.getcwd()

stat_files = [x for x in os.listdir(job_path) if re.match(r'.*.csv', x)]



class AccumulatorStatisticEntry:
    SimTime    = 0
    Rank    = 0
    Sum = 0
    SumSQ = 0
    Count = 0

    def hash(self):
        return {
            "SimTime": self.SimTime,
            "Rank": self.Rank,
            "Sum": self.Sum,
            "SumSQ": self.SumSQ,
            "Count": self.Count
        }

class Results:
    warnings = []
    errors   = []

    data     = {}

    def hash(self):
        return {
            "data":     self.data,
            "warnings": self.warnings,
            "errors":   self.errors
        }

def parse_results(f):
    results = Results()

    for line in f:
        line = line.strip()
        values = [ x.strip() for x in line.split(",") ]

        print(values)
        # FILLER TO IGNORE
        if values[0].startswith("ComponentName"):
            continue
        component_name=values[0]
        try:
            current_component = results.data[component_name][0]
        except Exception as e:
            results.data[component_name]=[{}]
            current_component = results.data[component_name][0]

        stat_name=values[1]

        ase = AccumulatorStatisticEntry()

        ase.SimTime=values[4]
        ase.Rank=values[5]
        ase.Sum=values[6]
        ase.SumSQ=values[7]
        ase.Count=values[8]

        current_component[stat_name] = ase.hash()
    return results

all_permutation=None
all_trace_file={}
simulator_name=""


# Create a new object
output_path="%s/new_output" % (object.path())

#Unless there is an output
outputs = object.outputs("application/json")
if len(outputs) > 0:
    output = outputs[0]
    output_path = output.volume()
else:
    os.mkdir(output_path)

if not os.path.exists(output_path):
    os.mkdir(output_path);






output = {"experiment":[]}
for stats in stat_files:
    name_info = ".".join(os.path.basename(stats).split(".")[0:-1])[10:].split("-")

    if(len(name_info)>2):
        permutation = eval(name_info[2])
        del name_info[2]

    trace_file=name_info[1]
    del name_info[1]

    simulator_name=name_info[0]
    del name_info[0]


    all_trace_file[trace_file]=1
    all_permutation=permutation


    f = open(os.path.join(job_path,stats), 'r')
    results = parse_results(f)
    f.close()

    output_data = OrderedDict(results.hash()["data"])

    i = open("config/%s/input.json"%(os.environ['OCCAM_OBJECT_INDEX']), "r")
    output_data["inputs"] = [json.load(i)]
    i.close()

    structure=ProcessInputFiles.get_structure()
    if len(structure)>0:
        for input in structure:
            type=input["type"]
            subtype=input["subtype"]
            files=input["files"]
            path=input["path"]
            field_name ="input_info"
            output_data[field_name]=output_data.get(field_name,[])
            if(type == "trace"):
                output_data[field_name].append({"trace":trace_file})
            elif(type == "configuration"):
                if(permutation!=None):
                    output_data[field_name].append({"permutation":permutation})
    output["experiment"].append(copy.deepcopy(output_data))



name=""
for k in all_trace_file.keys():
    if(name==""):
        name="%s_using_%s"%(simulator_name,os.path.basename(k))
    else:
        name="%s_and_%s"%(name,os.path.basename(k))

if all_permutation!=None:
    if all_permutation == True:
        name="%s_with_permutation"%(name)
    else:
        name="%s_without_permutation"%(name)


output_file = "%s.json"%(name)
i=0
while os.path.exists("%s/%s"%(output_path,output_file)):
    output_file = "%s%d.json"%(name,i)
    i+=1
o = open("%s/%s"%(output_path,output_file), "w+")
o.write(json.dumps(output, indent=4, separators=(',', ': ')))
o.close()

NewOutputInfo = OrderedDict({
    "name": name,
    "file": "%s"%(output_file),
    "schema": "output_schema.json",
    "type": "application/json"
})

f = open("%s/object.json" % (output_path), 'w+')
f.write(json.dumps(NewOutputInfo, indent=4, separators=(',', ': ')))
f.close()



# Report warnings/errors to OCCAM
#Occam.finish(results.hash()["warnings"], results.hash()["errors"])
