
export MPIHOME=`pwd`/local/packages/OpenMPI-1.8
export BOOST_HOME=`pwd`/local/packages/boost-1.56

# Compile DRAMSIM2 for building SST
export DRAMSIM2_HOME=`pwd`/dramsim2

# Check if Boost is already built
if [ ! -e DRAMSIM2_BUILT ]; then

    # Build SST
    cd dramsim2
    make libdramsim.so
    MAKE_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $MAKE_SUCCESS -eq 0 ]
    then
        touch DRAMSIM2_BUILT
    fi
else
    echo "Skipping build DRAMSIM2 ... Already built"
fi


#Install SST-core
export SST_HOME_CORE=`pwd`/local/sst-core
# Check if SST Core is already built
if [ ! -e SST_CORE_BUILT ]; then

    # Build SST
    cd sst-core
    if [ ! -e ./configure ]; then
        ./autogen.sh
    fi
    if [ ! -e ./Makefile ]; then
        ./configure --prefix=$SST_HOME_CORE --with-boost=$BOOST_HOME
    fi
    make all -j2
    make install
    INSTALL_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $INSTALL_SUCCESS -eq 0 ]
    then
        touch SST_CORE_BUILT
        rm -rf sst-core
    fi
else
    echo "Skipping build SST Core ... Already built"
fi

export SST_HOME_ELEMENTS=`pwd`/local/sst-elements

# Check if SST Elements is already built
if [ ! -e SST_ELEMENTS_BUILT ]; then

    # Build SST
    cd sst-elements
    if [ ! -e ./configure ]; then
        ./autogen.sh
    fi
    if [ ! -e ./Makefile ]; then
        ./configure --prefix=$SST_HOME_ELEMENTS --with-sst-core=$SST_HOME_CORE --with-dramsim=$DRAMSIM2_HOME
    fi
    make all -j2
    make install
    INSTALL_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $INSTALL_SUCCESS -eq 0 ]
    then
        touch SST_ELEMENTS_BUILT
        mkdir -p sst-git/sst/elements/simpleElementExample/tests/
        cp sst-elements/src/sst/elements/simpleElementExample/tests/test_simpleMessageGeneratorComponent.py sst-git/sst/elements/simpleElementExample/tests/test_simpleMessageGeneratorComponent.py
        rm -rf sst-elements
    fi
else
    echo "Skipping build SST Core ... Already built"
fi
