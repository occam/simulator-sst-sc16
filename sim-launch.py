import os
import subprocess
import json

from occam import Occam
from tools.file_tools.process_input_files import ProcessInputFiles
from collections import OrderedDict

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()

object = Occam.load()

# Get SST path
dependencies = object.dependencies()
sst_source_code_path = ""
for dependency in dependencies:
    dep_type = dependency.get("type","")
    dep_subtype = dependency.get("subtype","")
    dep_name = dependency.get("name","")
    if(dep_name == "SST") and (dep_type == "simulator-instantiator"):
        sst_source_code_path = "/occam/%s-%s" % (dependency.get("id",""), dependency.get("revision",""))

if( sst_source_code_path == "" ):
    print("Error: could not find SST source code")
else:
    print("Found SST at %s"%(sst_source_code_path))


binary_path     = os.path.join(sst_source_code_path, "local", "sst-core", "bin")
binary          = os.path.join(binary_path, "sst")

simulator_init_path = os.path.join(scripts_path, "simulator.py")
inject_path = os.path.join(scripts_path, "inject_simulation.in")

# Open object.json for command line options
input_data=object.configuration("General")
simulator_name = object.name()
# Generate input
# Get inputs to simulator object

structure=ProcessInputFiles.get_structure()

trace_input = None
permutation = None
if len(structure)>0:
    #for each trace file
    for input in structure:
        type=input["type"]
        subtype=input["subtype"]
        files=input["files"]
        path=input["path"]
        if (type=="trace"):
            trace_input = input
        if (type=="configuration"):
            with open(os.path.join(path,"system.ini"), "r") as fin:
                for line in fin:
                    l=line.replace(" ","").split('=')
                    if(l[0].upper()=="permutation".upper()):
                        if(int(l[1])==1):
                            permutation=True
                        else:
                            permutation=False
runs = []
if(trace_input!=None):
    type=trace_input["type"]
    subtype=trace_input["subtype"]
    files=trace_input["files"]
    path=trace_input["path"]
    for file in files:
        file_name=os.path.basename(file).replace("-","_")
        sim_new_filename = "simulator-%s-%s"%(simulator_name.replace(" ","_").replace("-","_"),file_name)
        sim_new_filename = "%s-%s"%(sim_new_filename, permutation)
        sim_new_file = os.path.join( scripts_path,"%s.py"%(sim_new_filename) )
        statistics_output_file = "%s.csv"%(sim_new_filename)
        with open(sim_new_file, "w+") as fout:
            with open(simulator_init_path, "r") as fin:
                for line in fin:
                    fout.write(line)
            with open(inject_path, "r") as fin:
                for line in fin:
                    fout.write(line.replace('@OUTPUT_STATISTICS_FILE@', statistics_output_file))

        run={}
        run["sim_file"]=sim_new_file
        run["trace"] = "--%s_%s%d %s" % (type, subtype, 0,file)
        runs.append(run)

config_parameters = OrderedDict({})
if len(structure)>0:
    config_parameters["begin"] = "--model-options '"
    config_parameters["contents"] =""
    config_parameters["end"] = "'"
    for input in structure:
        type=input["type"]
        subtype=input["subtype"]
        files=input["files"]
        path=input["path"]
        if(type!="trace"):
            for index in range(0,len(files)):
                file_path = files[index]
                config_parameters["contents"] = config_parameters["contents"] + " --%s_%s%d %s" % (type, subtype, index,file_path)
            config_parameters["contents"] = config_parameters["contents"] + " --%s_%s_path %s" % (type, subtype, path)

# Form arguments

# This command will run the SST simulator
command = []
for run in runs:

    sim_params = config_parameters["begin"] + config_parameters["contents"] + " " + run["trace"] + config_parameters["end"]
    exec_command = [binary,
        run["sim_file"],
        sim_params,
        "-v"]
    command.append(" ".join(exec_command))

# Form command to gather results
finish_binary = "python"
finish_args =   [   finish_binary,
                    os.path.join(scripts_path, "parse_outputs.py")
                ]
finish_command = ' '.join(finish_args)

# Tell OCCAM how to run the generated SST simulator
Occam.report(command, finish_command)
