import json
import re
import sys
import os
from collections import OrderedDict

from tools.schema_generation_utils import generate_input
from tools.schema_generation_utils import generate_output
from tools.schema_generation_utils import process_sst

# Input and output schemas
sstinfo_parameters=process_sst.get_sstinfo_parameters()
sstinfo_statistics=process_sst.get_sstinfo_statistics()
elements=process_sst.get_list_of_elements()
user_defaults=process_sst.get_user_defaults()

schema_input=generate_input.get_schema_manual(sstinfo_parameters, elements, user_defaults)
schema_output=generate_output.get_schema(sstinfo_statistics, elements)

schema_output["inputs"]=[schema_input]

# Input files
array_of_inputs = generate_input.get_files_manual(sstinfo_parameters, elements, user_defaults)
inp_files=[]
field_name ="input_info"
schema_output[field_name]=[]
for input in array_of_inputs:
    if( (input['type']=="trace") and (input['subtype']=="prospero") ):
        schema_output[field_name].append({"trace":{"units": " ", "type": "int"}})

    if( (input['type']=="configuration") and (input['subtype']=="DRAMSIM2") ):
        schema_output[field_name].append({"permutation":{"units": " ", "type": "int"}})

schema_output=OrderedDict({"experiment":[schema_output]})
#print(json.dumps(OrderedDict(sorted(schema_input.items(), key=lambda k: k[0])), indent=2, separators=(',', ': ')))


# Occam stuff
from occam import Occam
object = Occam.load()

# Directory where this file is located
scripts_path = os.path.dirname(__file__)
# Directory where outputs should be written
output_path  = object.path()

# Directory where the new simulator will be created
newSimPath = os.path.join(output_path, "new_simulator")
if not os.path.exists(newSimPath):
    os.mkdir(newSimPath);


configuration = object.configuration("General Options")

f = open(os.path.join(newSimPath, 'input_schema.json'), 'w+')
f.write(json.dumps(OrderedDict(sorted(schema_input.items(), key=lambda k: k[0])), indent=4, separators=(',', ': ')))
f.close()
f = open(os.path.join(newSimPath, 'output_schema.json'), 'w+')
f.write(json.dumps(OrderedDict(sorted(schema_output.items(), key=lambda k: k[0])), indent=4, separators=(',', ': ')))
f.close()


new_simulator_dependencies = [object._object]

# Write out object.json for new simulator
newSimulatorInfo = OrderedDict({
    "name": configuration['name'],
    "dependencies": new_simulator_dependencies,

    "architecture": "x86-64",
    "environment": "ubuntu:14.04",
    "run": {
        "script": "sim-launch.py",
        "version": "3.3.0",
        "language": "python"
    },

    "inputs": array_of_inputs,
    "configurations": [
        {
            "schema": "input_schema.json",
            "file": "input.json",
            "name": "General"
        }
    ],
    "outputs": [
        {
            "createIn": "new_output",
            "file": "output.json",
            "schema": "output_schema.json",
            "type": "application/json"
        }
    ]
})

f = open(os.path.join(newSimPath, 'object.json'), 'w+')
f.write(json.dumps(newSimulatorInfo, indent=4, separators=(',', ': ')))
f.close()

# Copy sim-launch.py
sim_launch_py_path = os.path.join(scripts_path, "sim-launch.py")
os.system('cp %s %s' % (sim_launch_py_path, os.path.join(newSimPath, '.')))

# Copy occam.py
occam_py_path = os.path.join(scripts_path, "occam.py")
os.system('cp %s %s' % (occam_py_path, os.path.join(newSimPath, '.')))

# Copy tools
occam_py_path = os.path.join(scripts_path, "tools")
os.system('cp -R %s %s' % (occam_py_path, os.path.join(newSimPath, '.')))

# Copy configuration script
simulator_py_path = "simulator.py"
os.system('cp %s %s' % (simulator_py_path, os.path.join(newSimPath, 'simulator.py')))
inject_simulation_path = os.path.join(scripts_path, "inject_simulation.in")
os.system('cp %s %s' % (inject_simulation_path, os.path.join(newSimPath, 'inject_simulation.in')))

# Copy parser
occam_parser_path = os.path.join(scripts_path, "parse_outputs.py")
os.system('cp %s %s' % (occam_parser_path, os.path.join(newSimPath, '.')))
