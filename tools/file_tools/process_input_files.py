import os
import json
import pdb
from occam import Occam
from collections import OrderedDict
class ProcessInputFiles:

    # Object obtained from occam.py: Occam.load().inputs()
    @staticmethod
    def get_structure():
        object=Occam.load()
        ret = []
        for input in object.inputs():
            #print(input)
            type=input._object["type"]
            #print(type)
            print(json.dumps(input._object.keys(), indent=2, separators=(',', ': ')))
            #pdb.set_trace()

            subtype=input._object["subtype"] if input._object.has_key("subtype") else "None"

            # TODO: This should not be necessary!!
            name=input._object["name"]
            if name == "generated":
                files = []
                files_in_dir=os.listdir(input.volume())
                for file in files_in_dir:
                    if file!="object.json":
                        files.append(os.path.join(input.volume(),file))
                print(files)
            else:
                files=input.files()

            path = input.volume()

            input_data = OrderedDict({})
            input_data["type"]=type
            input_data["subtype"]=subtype
            input_data["files"]=files
            input_data["path"]=path
            ret.append(input_data)

        print(json.dumps(ret, indent=2, separators=(',', ': ')))
        return ret
