import re
import os
import json
#############################################################
#                                                           #
#           Load information garthered from sstinfo         #
#                                                           #
#############################################################
def get_sstinfo_parameters(info_path="./sstinfo.txt"):

    f = open(info_path, 'r')

    sstinfo = {}

    currentElementHash = {}
    currentHash = {}
    for line in f:
      if line.startswith("ELEMENT "):
        elementName = line.split()[3]
        sstinfo[elementName] = {}
        currentElementHash = sstinfo[elementName]
      if line.startswith("      COMPONENT "):
        componentName = line.split()[3]
        currentElementHash[componentName] = {}
        currentComponentHash = currentElementHash[componentName]
        currentHash=currentComponentHash
        currentHash["parameters"] = {}
      if line.startswith("      SUBCOMPONENT "):
        subcomponentName = line.split()[3]
        currentElementHash[subcomponentName] = {}
        currentSubComponentHash = currentElementHash[subcomponentName]
        currentHash=currentSubComponentHash
        currentHash["parameters"] = {}
      if line.startswith("            PARAMETER"):
        parameterName = line.split()[3]
        default = line.split()[-1][1:-1]
        description = ' '.join(line.split()[4:-1])[1:-1]
        currentHash["parameters"][parameterName] = {}
        if default != "" and default != "REQUIRED":
          currentHash["parameters"][parameterName]["default"] = default
        if description != "":
          currentHash["parameters"][parameterName]["description"] = description
        currentHash["parameters"][parameterName]["type"] = "string"
    f.close()
    #print(json.dumps(sstinfo, indent=2, separators=(',', ': ')))
    return sstinfo

def get_sstinfo_statistics(info_path="./sstinfo.txt"):

    f = open(info_path, 'r')

    sstinfo = {}

    currentElementHash = {}
    currentHash = {}
    for line in f:
        if line.startswith("ELEMENT "):
            elementName = line.split()[3]
            sstinfo[elementName] = {}
            currentElementHash = sstinfo[elementName]
        if line.startswith("      COMPONENT ") or line.startswith("      SUBCOMPONENT "):
            componentName = line.split()[3]
            currentElementHash[componentName] = {}
            currentComponentHash = currentElementHash[componentName]
            currentHash=currentComponentHash
            currentHash["statistic"] = {}
        if line.startswith("            STATISTIC"):
            parameterName = line.split()[3]
            units = line.split()[4][1:-1]
            description = ' '.join(line.split()[5:-4])[1:-1]
            level = line.split()[-1]
            items = {
                "SimTime":
                {
                    "units":    units,
                    "type":     "int"
                },
                "Rank":
                {
                    "units":    units,
                    "type":     "int"
                },
                "Sum":
                {
                    "units":    units,
                    "type":     "int"
                },
                "SumSQ":
                {
                    "units":    units,
                    "type":     "int"
                },
                "Count":
                {
                    "units":    units,
                    "type":     "int"
                }
            }
            currentHash["statistic"][parameterName] = items

    f.close()
    #print(json.dumps(sstinfo, indent=2, separators=(',', ': ')))
    return sstinfo


def get_list_of_elements(component_path="./sst-partition-output.txt"):
    #############################################################
    #                                                           #
    #   Generate inputs for modules in the cofiguration         #
    #                                                           #
    #############################################################
    f = open(component_path, 'r')
    blocks={}
    exp = re.compile("^\s\s\s\S")
    for line in f:
        if exp.match(line):
            componentName = line.split()[0]
        elif line.startswith("      -> type"):
            componentString = ' '.join(line.split()[2:])
            blocks[componentName]=[]
            blocks[componentName].append(componentString)
    f.close()
    return blocks

def get_user_defaults(config_path="./sst-json-output.txt"):
    #############################################################
    #                                                           #
    #   Read defaults set by the user in simulation config      #
    #                                                           #
    #############################################################

    output = {}

    f = open(config_path, 'r')
    json_input = ''
    for line in f:
        json_input += line
    json_dic = json.loads(json_input)
    for value in json_dic["components"]:
        #print(json.dumps(value, indent=2, separators=(',',': ') ))
        #print("---------------------------------------------------------------")
        name = value["name"]
        output[name]={}
        for param in value["params"]:
            output[name][param["name"]] = param["value"]
        #print(json.dumps(output, indent=2, separators=(',',': ') ))
        #print("===============================================================")
    f.close()
    return output
