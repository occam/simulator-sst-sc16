import re
import os
import json
from collections import OrderedDict
#############################################################

def get_schema(sstinfo, blocks):
    schema_output = OrderedDict({})
    for blockName in blocks:
        schema_output[blockName] = [OrderedDict({})]
        current_output_component = schema_output[blockName][0]
        for types in blocks[blockName]:
            separate_types=types.split('.')
            element=separate_types[0]
            component=separate_types[1]
            if element in sstinfo:
                elementInfo = sstinfo[element]
                for component, statistics in elementInfo.items():
                    for k,v in statistics["statistic"].items():
                        current_output_component[k] = v
            else:
                print("Error: cannot find module %s" % (module))
    return schema_output
