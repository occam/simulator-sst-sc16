import re
import os
import json
from collections import OrderedDict

def get_schema(sstinfo, blocks):
    schema_input = OrderedDict({})
    for blockName in blocks:
        schema_input[blockName] = OrderedDict({})
        current_input_component = schema_input[blockName]
        for types in blocks[blockName]:
            separate_types=types.split('.')
            element=separate_types[0]
            component=separate_types[1]
            if element in sstinfo:
                elementInfo = sstinfo[element]
                if component in elementInfo:
                    componentInfo = elementInfo[component]
                    for k,v in componentInfo["parameters"].items():
                        current_input_component[k] = v
                else:
                    print("Error: cannot find component %s in element %s" % (component, element))
            else:
                print("Error: cannot find module %s" % (module))

    return schema_input

def get_schema_manual(sstinfo, blocks, user_defaults={}):
    schema_input=OrderedDict({})
    for blockName in blocks:
        schema_input[blockName] = OrderedDict({})
        current_input_component = schema_input[blockName]
        for types in blocks[blockName]:
            separate_types=types.split('.')
            element=separate_types[0]
            component=separate_types[1]
            print(element)
            print(component)
            if( ( element == 'prospero' ) and ( component == 'prosperoCPU' ) ):
                if blockName in user_defaults.keys():
                    component_user_defaults = user_defaults[blockName]
                else:
                    component_user_defaults=OrderedDict({})
                current_input_component.update(dump_prospero(component_user_defaults))
            elif( element == 'memHierarchy' ):
                if( component == 'MemController' ):
                    if blockName in user_defaults.keys():
                        component_user_defaults = user_defaults[blockName]
                    else:
                        component_user_defaults=OrderedDict({})
                    current_input_component.update(dump_MemController(component_user_defaults))
                elif( component == 'Cache' ):
                    if blockName in user_defaults.keys():
                        component_user_defaults = user_defaults[blockName]
                    else:
                        component_user_defaults=OrderedDict({})
                    current_input_component.update(dump_Cache(component_user_defaults))

    return schema_input

def get_files_manual(sstinfo, blocks, user_defaults={}):
    files=[]
    for blockName in blocks:
        for types in blocks[blockName]:
            separate_types=types.split('.')
            element=separate_types[0]
            component=separate_types[1]
            if( ( element == 'prospero' ) and ( component == 'prosperoCPU' ) ):
                files.append({
                  "type": "trace",
                  "subtype": "prospero"
                })
            elif( ( element == 'memHierarchy' ) and ( component == 'MemController' ) ):
                if blockName in user_defaults.keys():
                    if user_defaults[blockName]["backend"] == "memHierarchy.dramsim":
                        files.append({
                          "type": "configuration",
                          "subtype": "DRAMSIM2"
                        })
    return files

def dump_prospero(user_defaults={}):
    #print(user_defaults)
    #print("++++++++++++++++++++++++++++++")
    #Cannot use "." in OCCAM...
    new_user_defaults = OrderedDict({})
    for k in user_defaults.keys():
        new_user_defaults[k.replace(".", "::")]=user_defaults[k]
    user_defaults=new_user_defaults

    prospero_config=OrderedDict({})
    component = "cache_line_size"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 64
    prospero_config[component]={
        "default": default,
        "type": "int",
        "description": "Sets the length of the cache line in bytes, this should match the L1 cache"
    }

    component = "verbose"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    prospero_config[component]={
        "default": default,
        "type": "int",
        "description": "Verbosity for debugging. Increased numbers for increased verbosity."
    }

    component = "pagesize"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 4096
    prospero_config[component]={
        "default": default,
        "type": "int",
        "description": "Sets the page size for the Prospero simple virtual memory manager"
    }

    component = "clock"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 2
        default_unit="GHz"
    prospero_config[component]={
        "default": default,
        "type": "int",
        "description": "Sets the clock of the core"
    }
    prospero_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the clock parameter (e.g. GHz)"
    }

    component = "max_outstanding"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 16
    prospero_config[component]={
        "default": default,
        "type": "int",
        "description": "Sets the maximum number of outstanding transactions that the memory system will allow"
    }

    component = "max_issue_per_cycle"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 2
    prospero_config[component]={
        "default": default,
        "type": "int",
        "description": "Sets the maximum number of new transactions that the system can issue per cycle"
    }

    component = "reader"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "prospero.ProsperoTextTraceReader"
    prospero_config[component]={
        "default": default,
        "type": [
            "prospero.ProsperoTextTraceReader",
            "prospero.ProsperoBinaryTraceReader",
            "prospero.ProsperoCompressedBinaryTraceReader"
        ],
        "description": "The trace reader module to load"
        # "shows":[
        #     {
        #         "is":"prospero.ProsperoTextTraceReader",
        #         "key":"ProsperoTextTraceReader"
        #     },
        #     {
        #         "is":"prospero.ProsperoBinaryTraceReader",
        #         "key":"ProsperoBinaryTraceReader"
        #     },
        #     {
        #         "is":"prospero.ProsperoCompressedBinaryTraceReader",
        #         "key":"ProsperoCompressedBinaryTraceReader"
        #     }
        # ]
    }



    # prospero_config['ProsperoTextTraceReader']={}
    # prospero_config['ProsperoTextTraceReader']['file']={"default": "File must be added as input to the simulator block","type": "string","description": "Reads a trace from a text file"}
    # prospero_config['ProsperoBinaryTraceReader']={}
    # prospero_config['ProsperoBinaryTraceReader']['file']={"default": "File must be added as input to the simulator block","type": "string","description": "Reads a trace from a binary file"}
    # prospero_config['ProsperoCompressedBinaryTraceReader']={}
    # prospero_config['ProsperoCompressedBinaryTraceReader']['file']={"default": "File must be added as input to the simulator block","type": "string","description": "Reads a trace from a compressed binary file"}
    return prospero_config

def dump_MemController(user_defaults={}):
    #print(user_defaults)
    #print("++++++++++++++++++++++++++++++")
    #Cannot use "." in OCCAM...
    new_user_defaults = OrderedDict({})
    for k in user_defaults.keys():
        new_user_defaults[k.replace(".", "::")]=user_defaults[k]
    user_defaults=new_user_defaults

    MemController_config=OrderedDict({})
    component = "backend::mem_size"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Size of physical memory in MiB"
    }

    component = "clock"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 1
        default_unit="GHz"
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Clock frequency of controller"
    }
    MemController_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the clock frequency of controller (e.g. GHz)"
    }



    component = "backend"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "memHierarchy::simpleMem"
    MemController_config[component]={
        "default": default,
        "type": [default],#[
            #"memHierarchy::simpleMem",
            #"memHierarchy::simpleDRAM"
            #"memHierarchy::dramsim"
        #],
        "description": "Timing backend to use:  Default to simpleMem",
        "shows":[
            {
                "is":"memHierarchy::simpleMem",
                "key":"simpleMem"
            },
            {
                "is":"memHierarchy::simpleDRAM",
                "key":"simpleDRAM"
            },
            {
                "is":"memHierarchy::dramsim",
                "key":"dramsim"
            }
        ]
    }
    if(default == 'memHierarchy::simpleMem'):
        component = "backend::verbose"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 0
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Sets the verbosity of the backend output"
        }

        component = "backend::access_time"
        component_unit = "%s_OCCAM_unit" % (component)
        if component in user_defaults.keys():
            for i,c in enumerate(user_defaults[component]):
                if not c.isdigit():
                    break
            default=int(user_defaults[component][:i])
            default_unit=user_defaults[component][i:]
        else:
            default = 100
            default_unit="ns"
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Constant latency of memory operation."
        }
        MemController_config[component_unit]={
            "default": default_unit,
            "type": "string",
            "description": "Sets the unit of the latency of memory operation (e.g. ns)"
        }

    if(default == 'memHierarchy.simpleDRAM'):
        component = "backend::verbose"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 0
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Sets the verbosity of the backend output"
        }

        component = "backend::cycle_time"
        component_unit = "%s_OCCAM_unit" % (component)
        if component in user_defaults.keys():
            for i,c in enumerate(user_defaults[component]):
                if not c.isdigit():
                    break
            default=int(user_defaults[component][:i])
            default_unit=user_defaults[component][i:]
        else:
            default = 4
            default_unit="ns"
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Latency of a cycle or clock frequency (e.g., '4ns' and '250MHz' are both accepted)"
        }
        MemController_config[component_unit]={
            "default": default_unit,
            "type": "string",
            "description": "Sets the unit of the latency of a cycle or clock frequency (e.g., '4ns' and '250MHz' are both accepted)"
        }

        component = "backend::tCAS"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 9
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Column access latency in cycles (i.e., access time if correct row is already open)"
        }

        component = "backend::tRCD"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 9
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Row access latency in cycles (i.e., time to open a row)"
        }

        component = "backend::tRP"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 9
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Precharge delay in cycles (i.e., time to close a row)"
        }

        component = "backend::banks"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 8
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Number of banks"
        }

        component = "backend::bank_interleave_granularity"
        component_unit = "%s_OCCAM_unit" % (component)
        if component in user_defaults.keys():
            for i,c in enumerate(user_defaults[component]):
                if not c.isdigit():
                    break
            default=int(user_defaults[component][:i])
            default_unit=user_defaults[component][i:]
        else:
            default = 64
            default_unit="B"
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Granularity of interleaving in bytes (B), generally a cache line. Must be a power of 2.",
            "validations": [
                {
                    "test":    "log2(x) == floor(log2(x))",
                    "message": "Must be a power of two"
                }
            ]
        }
        MemController_config[component_unit]={
            "default": default_unit,
            "type": "string",
            "description": "Sets the unit of interleaving"
        }

        component = "backend::row_size"
        component_unit = "%s_OCCAM_unit" % (component)
        if component in user_defaults.keys():
            for i,c in enumerate(user_defaults[component]):
                if not c.isdigit():
                    break
            default=int(user_defaults[component][:i])
            default_unit=user_defaults[component][i:]
        else:
            default = 8
            default_unit="KiB"
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Size of a row in bytes (B). Must be a power of 2.",
            "validations": [
                {
                    "test":    "log2(x) == floor(log2(x))",
                    "message": "Must be a power of two"
                }
            ]
        }
        MemController_config[component_unit]={
            "default": default_unit,
            "type": "string",
            "description": "Sets the unit of a row"
        }


        component = "backend::row_policy"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = "closed"
        MemController_config[component]={
            "default": default,
            "type": ["closed", "open"],
            "description": "Policy for managing the row buffer - open or closed."
        }

        component = "backend::permutation"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 0
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Option to activate or deactivate address permutation.",
            "validations": [
                {
                "test":    "x<=1",
                "message": "Must be either 1 or 0"
                },
                {
                "test":    "x>=0",
                "message": "Must be either 1 or 0"
                }
            ]
        }
    if(default == 'memHierarchy::dramsim'):
        component = "backend::verbose"
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = 0
        MemController_config[component]={
            "default": default,
            "type": "int",
            "description": "Sets the verbosity of the backend output"
        }


    component = "coherence_protocol"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "MESI"
    MemController_config[component]={
        "default": default,
        "type": ["MESI","MSI"],
        "description": "Coherence protocol.  Supported: MESI (default), MSI. Only used when a directory controller is not present."
    }

    component = "request_width"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 64
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Size of a DRAM request in bytes."
    }

    component = "max_requests_per_cycle"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 1
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Maximum number of requests to accept per cycle. 0 or negative is unlimited."
    }

    component = "range_start"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Address where physical memory begins"
    }

    component = "interleave_step"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 0
        default_unit="B"
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Size of interleaved chunks in bytes with units (e.g., 64B or 4KiB). Note: This definition has CHANGED (used to be specified in KiB)"
    }
    MemController_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of interleaved chunks"
    }

    component = "direct_link_latency"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 10
        default_unit="ns"
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "direct_link_latency (Latency when using the 'direct_link', rather than 'snoop_link')"
    }
    MemController_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of interleaved chunks"
    }

    #PARAMETER 10 = memory_file (Optional backing-store file to pre-load memory, or store resulting state) [N/A]
    #PARAMETER 11 = trace_file (File name (optional) of a trace-file to generate.) []
    #PARAMETER 12 = debug (0 (default): No debugging, 1: STDOUT, 2: STDERR, 3: FILE.) [0]
    #PARAMETER 13 = debug_level (Debugging level: 0 to 10) [0]
    #PARAMETER 14 = debug_addr (Optional, int      - Address (in decimal) to be debugged, if not specified or specified as -1, debug output for all addresses will be printed) [-1]

    component = "listenercount"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    MemController_config[component]={
        "default": default,
        "type": [default],
        "description": "Counts the number of listeners attached to this controller, these are modules for tracing or components like prefetchers."
    }
    for i in range(0,default):
        component = "listener%d" (i)
        if component in user_defaults.keys():
            default = user_defaults[component]
        else:
            default = ""
        MemController_config[component]={
            "default": default,
            "type": "string",
            "description": "Loads a listener module into the controller"
        }

    component = "network_bw"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 80
        default_unit="GiB/s"
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Network link bandwidth)"
    }
    MemController_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of network link bandwidth"
    }

    component = "network_address"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Network address of component."
    }

    component = "network_input_buffer_size"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 1
        default_unit="KiB"
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Size of the network's input buffer."
    }
    MemController_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the network's input buffer."
    }

    component = "network_output_buffer_size"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 1
        default_unit="KiB"
    MemController_config[component]={
        "default": default,
        "type": "int",
        "description": "Size of the network's output buffer."
    }
    MemController_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the network's output buffer."
    }

    #PARAMETER 21 = do_not_back (DO NOT use this parameter if simulation depends on correct memory values. Otherwise, set to '1' to reduce simulation's memory footprint) [0]
    # PARAMETER 22 = mem_size (DEPRECATED. Use 'backend.mem_size' instead. Size of physical memory in MiB) [0]
    # PARAMETER 23 = statistics (DEPRECATED - use Statistics API to get statistics for memory controller) [0]
    # PARAMETER 24 = network_num_vc (DEPRECATED. Number of virtual channels (VCs) on the on-chip network. memHierarchy only uses one VC.) [1]
    # PARAMETER 25 = direct_link (DEPRECATED. Now auto-detected by configure. Specifies whether memory is directly connected to a directory/cache (1) or is connected via the network (0)) [1]

    return MemController_config



def dump_Cache(user_defaults={}):

    new_user_defaults = OrderedDict({})
    for k in user_defaults.keys():
        new_user_defaults[k.replace(".", "::")]=user_defaults[k]
    user_defaults=new_user_defaults

    Cache_config=OrderedDict({})



    component = "cache_frequency"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 1
        default_unit="GHz"
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Clock frequency. For L1s, this is usually the same as the CPU's frequency."
    }
    Cache_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the cache_frequency  (e.g. GHz)"
    }

    component = "cache_size"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 32
        default_unit="KiB"
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Cache size."
    }
    Cache_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Cache size with units. Eg. 4KiB or 1MiB"
    }


    component = "associativity"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 8
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Associativity of the cache. In set associative mode, this is the number of ways."
    }

    component = "access_latency_cycles"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 6
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Latency (in cycles) to access the cache array."
    }

    component = "L1"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Required for L1s, specifies whether cache is an L1. Options: 0[not L1], 1[L1]."
    }


    component = "LL"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Required for LLCs without a directory below - indicates LLC is the lowest-level coherence entity. Options: 0[not LL entity], 1[LL entity]."
    }


    component = "cache_line_size"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 64
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Size of a cache line (aka cache block) in bytes."
    }

    component = "hash_function"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "0 - none (default), 1 - linear, 2 - XOR."
    }

    component = "coherence_protocol"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "MESI"
    Cache_config[component]={
        "default": default,
        "type": ["MESI", "MSI", "NONE"],
        "description": "Coherence protocol."
    }

    component = "replacement_policy"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "LRU"
    Cache_config[component]={
        "default": default,
        "type": ["LRU", "LFU", "Random", "MRU", "NMRU"],
        "description": "Replacement policy of the cache array. Options:  LRU[least-recently-used], LFU[least-frequently-used], Random, MRU[most-recently-used], or NMRU[not-most-recently-used]."
    }

    component = "cache_type"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "inclusive"
    Cache_config[component]={
        "default": default,
        "type": ["inclusive", "noninclusive", "noninclusive_with_directory"],
        "description": "Cache type. Options: inclusive cache ('inclusive', required for L1s), non-inclusive cache ('noninclusive') or non-inclusive cache with a directory ('noninclusive_with_directory', required for non-inclusive caches with multiple upper level caches directly above them)."
    }

    component = "max_requests_per_cycle"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = -1
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Maximum number of requests to accept per cycle. 0 or negative is unlimited."
    }

    component = "noninclusive_directory_repl"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "LRU"
    Cache_config[component]={
        "default": default,
        "type": ["LRU", "LFU", "MRU", "NMRU", "RANDOM"],
        "description": "If non-inclusive directory exists, its replacement policy. LRU, LFU, MRU, NMRU, or RANDOM."
    }

    component = "noninclusive_directory_entries"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Number of entries in the directory. Must be at least 1 if the non-inclusive directory exists."
    }

    component = "noninclusive_directory_associativity"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 1
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "For a set-associative directory, number of ways."
    }

    component = "lower_is_noninclusive"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Next lower level cache is non-inclusive, changes some coherence decisions (e.g., write back clean data). 0 (false), 1 (true)"
    }

    component = "mshr_num_entries"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = -1
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Number of MSHR entries. Not valid for L1s because L1 MSHRs assumed to be sized for the CPU's load/store queue. Setting this to -1 will create a very large MSHR."
    }

    component = "stat_group_ids"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = ""
    Cache_config[component]={
        "default": default,
        "type": "string",
        "description": "Stat grouping. Instructions with same IDs will be grouped for stats. Separated by commas."
    }

    # component = "tag_access_latency_cycles"
    # if component in user_defaults.keys():
    #     default = user_defaults[component]
    # else:
    #     default = access_latency_cycles
    # Cache_config[component]={
    #     "default": default,
    #     "type": "int",
    #     "description": "Latency (in cycles) to access tag portion only of cache. If not specified, defaults to access_latency_cycles."
    # }

    component = "mshr_latency_cycles"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = -1
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Latency (in cycles) to process responses in the cache (MSHR response hits). If not specified, simple intrapolation is used based on the cache access latency."
    }

    component = "prefetcher"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = ""
    Cache_config[component]={
        "default": default,
        "type": "string",
        "description": "Name of prefetcher module."
    }

    # component = "max_outstanding_prefetch"
    # if component in user_defaults.keys():
    #     default = user_defaults[component]
    # else:
    #     default = 0.5*mshr_num_entries
    # Cache_config[component]={
    #     "default": default,
    #     "type": "int",
    #     "description": "Maximum number of prefetch misses that can be outstanding, additional prefetches will be dropped/NACKed. Default is 1/2 of MSHR entries."
    # }

    # component = "drop_prefetch_mshr_level"
    # if component in user_defaults.keys():
    #     default = user_defaults[component]
    # else:
    #     default = mshr_num_entries-2
    # Cache_config[component]={
    #     "default": default,
    #     "type": "int",
    #     "description": "Drop/NACK prefetches if the number of in-use mshrs is greater than or equal to this number. Default is mshr_num_entries - 2."
    # }


    component = "num_cache_slices"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 1
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "For a distributed, shared cache, total number of cache slices."
    }


    component = "slice_id"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "For distributed, shared caches, unique ID for this cache slice."
    }

    component = "slice_allocation_policy"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = "rr"
    Cache_config[component]={
        "default": default,
        "type": ["rr"],
        "description": "Policy for allocating addresses among distributed shared cache. Options: rr[round-robin]."
    }


    component = "network_bw"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 80
        default_unit="GiB/s"
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "When connected to a network, the network link bandwidth."
    }
    Cache_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the network link bandwidth (e.g. GiB/s)"
    }


    component = "network_address"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "When connected to a network, the network address of this cache."
    }



    component = "network_input_buffer_size"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 1
        default_unit="KiB"
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "When connected to a network, size of the network's input buffer."
    }
    Cache_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the network's input buffer (e.g. KiB)"
    }

    component = "network_output_buffer_size"
    component_unit = "%s_OCCAM_unit" % (component)
    if component in user_defaults.keys():
        for i,c in enumerate(user_defaults[component]):
            if not c.isdigit():
                break
        default=int(user_defaults[component][:i])
        default_unit=user_defaults[component][i:]
    else:
        default = 1
        default_unit="KiB"
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "When connected to a network, size of the network's output buffer."
    }
    Cache_config[component_unit]={
        "default": default_unit,
        "type": "string",
        "description": "Sets the unit of the network's output buffer (e.g. KiB)"
    }


    component = "maxRequestDelay"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Set an error timeout if memory requests take longer than this in ns (0: disable)."
    }

    component = "snoop_l1_invalidations"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Forward invalidations from L1s to processors. Options: 0[off], 1[on]."
    }

    component = "debug"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Print debug information. Options: 0[no output], 1[stdout], 2[stderr], 3[file]."
    }

    component = "debug_level"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Debugging level. Between 0 and 10."
    }


    component = "debug_addr"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = -1
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Address (in decimal) to be debugged, if not specified or specified as -1, debug output for all addresses will be printed."
    }


    component = "force_noncacheable_reqs"
    if component in user_defaults.keys():
        default = user_defaults[component]
    else:
        default = 0
    Cache_config[component]={
        "default": default,
        "type": "int",
        "description": "Used for verification purposes. All requests are considered to be 'noncacheable'. Options: 0[off], 1[on]."
    }


    #PARAMETER 36 = LLC (DEPRECATED - Now auto-detected by configure. Specifies whether cache is a last-level cache. Options: 0[not LLC], 1[LLC]) [REQUIRED]
    #PARAMETER 37 = statistics (DEPRECATED - Use Statistics API to get statistics for caches.) [0]
    #PARAMETER 38 = network_num_vc (DEPRECATED - Number of virtual channels (VCs) on the on-chip network. memHierarchy only uses one VC.) [1]
    #PARAMETER 39 = directory_at_next_level (DEPRECATED - Now auto-detected by configure. Specifies if there is a directory-controller as the next lower memory level; deprecated -     set 'bottom_network' to 'directory' instead) [0]
    #PARAMETER 40 = bottom_network (DEPRECATED - Now auto-detected by configure. Specifies whether the cache is connected to a network below and the entity type of the connection.     Options: cache, directory, ''[no network below]) []
    #PARAMETER 41 = top_network (DEPRECATED - Now auto-detected by configure. Specifies whether the cache is connected to a network above and the entity type of the connection. Options: cache, ''[no network above]) []

    return Cache_config


##
