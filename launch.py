import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()

object = Occam.load()

sst_source_code_path = "/occam/%s-%s" % (object.id(), object.revision())

input_path = os.path.join(sst_source_code_path, "sst-git", "sst", "elements", "simpleElementExample", "tests", "test_simpleMessageGeneratorComponent.py")

#########################################################
#                                                       #
#       The only input is the configuration file        #
#                                                       #
#########################################################
inputs = object.inputs()
if len(inputs) > 0:
    files = inputs[0].files()

    if len(files) > 0:
        input_path = inputs[0].files()[0]

#Copy file to the new_simulator folder
path_to_new_simulator = "."#os.path.join(job_path, "new_simulator")
if not os.path.exists(path_to_new_simulator):
    os.mkdir(path_to_new_simulator);
os.system("cp %s %s" % (input_path, os.path.join(path_to_new_simulator, "simulator.py")))


#########################################################
#                                                       #
#       Generate sstinfo output for processing          #
#                                                       #
#########################################################
binary_path     = os.path.join(sst_source_code_path, "local", "sst-core", "bin")

sst_info_binary = os.path.join(binary_path, "sst-info")
info_args = [sst_info_binary,
             ">"
             "sstinfo.txt"]

command = ' '.join(info_args)
os.system(command)

# This command will generate the graph for the given SST input file.
binary = os.path.join(binary_path, "sst")
args = [binary,
        input_path,
        "--run-mode",
        "init",
        "-v",
        "--output-partition",
        "sst-partition-output.txt",
        "--output-json",
        "sst-json-output.txt"]

# Form command line
command = ' '.join(args)

# Form command to gather results
finish_command = "python %s/compile.py" % (scripts_path)

# Tell OCCAM how to run SST
Occam.report(command, finish_command)
