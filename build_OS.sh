
# Create installation path
mkdir -p `pwd`/local
mkdir -p `pwd`/local/lib
mkdir -p `pwd`/local/packages

# Get OpenMPI 1.8
# Determine install path for OpenMPI
export MPIHOME=`pwd`/local/packages/OpenMPI-1.8
echo "export MPIHOME=$MPIHOME"       >> /etc/bash.bashrc

# Check if MPI is already built
if [ ! -e MPI_BUILT ]; then
    # Unpack
    if [ ! -f openmpi-1.8 ]; then
        tar -xvf openmpi-1.8.tar.gz

        # Build OpenMPI
        cd openmpi-1.8
        ./configure --prefix=$MPIHOME
        make all
        make install
        INSTALL_SUCCESS=$?
        cd ..
        # Make sure make install succeded
        if [ $INSTALL_SUCCESS -eq 0 ]
        then
            touch MPI_BUILT
            rm -rf openmpi-1.8.tar.gz openmpi-1.8
        fi
    fi
else
    echo "Skipping build openmpi ... Already built"
fi

#Update PATH
export PATH=$MPIHOME/bin:$PATH
export MPICC=mpicc
export MPICXX=mpicxx
echo 'export PATH=$MPIHOME/bin:$PATH'  >> /etc/bash.bashrc
echo 'export MPICC=mpicc'              >> /etc/bash.bashrc
echo 'export MPICXX=mpicxx'            >> /etc/bash.bashrc



# Get Boost 1.56

# Determine install path for Boost
export BOOST_HOME=`pwd`/local/packages/boost-1.56
echo "export BOOST_HOME=$BOOST_HOME" >> /etc/bash.bashrc

# Check if Boost is already built
if [ ! -e BOOST_BUILT ]; then

    # Unpack
    if [ ! -f boost_1_56_0 ]; then
        tar -xvf boost_1_56_0.tar.gz

        # Build Boost
        cd boost_1_56_0
        ./bootstrap.sh --prefix=$BOOST_HOME
        ./b2 install
        INSTALL_SUCCESS=$?
        cd ..
        # Make sure make install succeded
        if [ $INSTALL_SUCCESS -eq 0 ]
        then
            touch BOOST_BUILT
            rm -rf boost_1_56_0.tar.gz boost_1_56_0
        fi
    fi
else
    echo "Skipping build Boost ... Already built"
fi
